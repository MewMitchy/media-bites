# Mediabites #

## Installation ##

### Environment ###
Don't forget to change the .env database **AND** facebook callback URI.


```
#!

APP_ENV=local
APP_DEBUG=true
APP_KEY=

DB_HOST=localhost
DB_DATABASE=
DB_USERNAME=root
DB_PASSWORD=

FACEBOOK_CLIENT_ID=777136172430667
FACEBOOK_CLIENT_SECRET=
FACEBOOK_CALLBACK_URL=http://localhost:8000/auth/facebook/callback

LINKEDIN_CLIENT_ID=77u394qvhg81m7
LINKEDIN_CLIENT_SECRET=
LINKEDIN_CALLBACK_URL=http://localhost:8000/auth/linkedin/callback

CACHE_DRIVER=file
SESSION_DRIVER=file
QUEUE_DRIVER=sync

MAIL_DRIVER=smtp
MAIL_HOST=mailtrap.io
MAIL_PORT=2525
MAIL_USERNAME=null
MAIL_PASSWORD=null
```


### Facebook Login ###
https://developers.facebook.com/apps/777136172430667/settings/basic/