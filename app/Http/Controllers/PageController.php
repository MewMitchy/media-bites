<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use Redirect;
use View;
use Auth;
use Input;
use Hash;

class PageController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Home Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders your application's "dashboard" for users that
	| are authenticated. Of course, you are free to change or remove the
	| controller as you wish. It is just here to get your app started!
	|
	*/

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function test($id)
	{
		$user = User::findOrFail($id);

		return view('pages/test', compact('user'));
	}

	public function home()
	{
		if(Auth::user()->role == '1'){
			Auth::user()->role = 'Student';
		}
		elseif(Auth::user()->role == '2'){
			Auth::user()->role = 'Professional';
		}
		elseif(Auth::user()->role == '0'){
			return view('select');
		}
		
		$encrypter = app('Illuminate\Encryption\Encrypter');
		$encrypted_token = $encrypter->encrypt(csrf_token());

	  	return view('home', compact('encrypted_token'));
	}

	public function select()
	{
		return view('select');
	}

	public function profile($slug)
	{
		$user = User::where('slug', '=' ,$slug)->first();

		if(!$user){
			return view('errors/profile');
		}
		else{
			if($user->role == '1'){
				$user->role = 'Student';
			}
			elseif($user->role == '2'){
				$user->role = 'Professional';
			}

			return view('pages/profile', compact('user'));
		}
	}

	public function users(){
		$users = User::where('role', '!=' ,Auth::user()->role)->get();

		return view('pages/list', compact('users'));
	}

	public function update($id, Request $request)
	{
		$user = User::findOrFail($id);

		$user->update($request->all());

		return redirect('/home');
	}

	public function updateAJAX($id, Request $request)
	{
		if($request->ajax())
		{
      		$user = User::findOrFail($id);

			$user->update($request->all());
		}
	}

	public function logout(){
		Auth::logout();

		return redirect('/');
	}

}
