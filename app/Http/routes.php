<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'WelcomeController@index');

Route::get('home', 'HomeController@index');

Route::get('auth/facebook/logout', 'PageController@logout');

Route::get('test/{id}', 'PageController@test');
// Route::put('test/{id}', array('as' => 'update', 'uses' => 'PageController@update'));
// Route::patch('test/{id}', 'PageController@update');

Route::put('test/{id}', array('as' => 'update', 'uses' => 'PageController@updateAJAX'));
Route::patch('test/{id}', 'PageController@updateAJAX');

Route::post('test/{id}', 'PageController@updateAJAX');

Route::get('users', 'PageController@users');
Route::get('user/{slug}', 'PageController@profile');

Route::get('home', array('as' => 'home', 'uses' => 'PageController@home'));
Route::get('select', array('as' => 'select', 'uses' => 'PageController@select'));

Route::get('auth/facebook', 'Auth\AuthController@redirectToProvider');
Route::get('auth/facebook/callback', 'Auth\AuthController@handleProviderCallback');