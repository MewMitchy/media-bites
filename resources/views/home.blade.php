@include('layouts/head')
@include('layouts/header')
<div class="container">
			@if(Auth::user())
				<a class="btn btn-default btn-sm" href="auth/facebook/logout" style="position: absolute;right: 2em;top: 1em;"><i class="glyphicon glyphicon-log-out" style="padding-right: 1em;"></i>Logout</a>
			@endif
			<div class="content container" style="width:100%">
				<div class="title">Media Bites</div>
			</div>

			<img src="{{Auth::user()->avatar}}" width="150" height="150" class="profile-picture">
			<h1>Welkom, <span class="profile-name">{{Auth::user()->name}}</span></h1>
			@if(Auth::user()->role != '0')
				<h2>{{Auth::user()->role}}</h2>
			@endif
			<div class="col-md-8 col-md-offset-2">
				<hr/>
			</div>

			@if(Auth::user()->role != '0')
				<h3 style="clear:both">Hieronder kunt u uw profiel aanpassen</h3>

				<div class="col-md-8 col-md-offset-2 form-container">
					{!!	Form::model(Auth::user(), ['route' => ['update', Auth::user()->id], 'method' => 'patch', 'id' => 'update-form'])	!!}
						<div class="form-group">
							{!!	Form::label('name', 'Naam')	!!}
							{!!	Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Vul in voor hoeveel dagen de prijs geldt', 'id' => 'form-name']) !!}
						</div>

						<div class="form-group">
							{!!	Form::submit('Sla veranderingen op', ['class' => 'btn btn-primary form-control form-submit-button']) !!}
						</div>

						<input id="token" type="hidden" value="{{$encrypted_token}}">

					{!!	Form::close() !!}
				</div>
			@endif
		</div>

@include('layouts/footer')

