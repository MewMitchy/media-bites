@include('layouts/head')
@include('layouts/header')
<div class="container">
			@if(Auth::user())
				<a class="btn btn-default btn-sm" href="auth/facebook/logout" style="position: absolute;right: 2em;top: 1em;"><i class="glyphicon glyphicon-log-out" style="padding-right: 1em;"></i>Logout</a>
			@endif
			<div class="content container" style="width:100%">
				<div class="title">Media Bites</div>
			</div>

			<img src="{{$user->avatar}}" width="150" height="150" class="profile-picture">
			<h1>{{$user->name}}</h1>
			<h1>{{$user->role}}</h1>
			@if($user->role == "1")
				<h2>Student</h2>
			@elseif($user->role == "2")
				<h2>Iemand uit het bedrijfsleven</h2>
			@endif
		</div>

@include('layouts/footer')