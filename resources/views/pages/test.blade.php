@include('layouts/head')
@include('layouts/header')
<?php
$encrypter = app('Illuminate\Encryption\Encrypter');
$encrypted_token = $encrypter->encrypt(csrf_token());
?>
<div class="container">
			<div class="content container" style="width:100%">
				<div class="title">Media Bites</div>
			</div>

			<img src="{{$user->avatar}}" width="150" height="150" class="profile-picture">
			<h1>Welkom, <span class="profile-name">{{$user->name}}</span></h1>
			<div class="col-md-8 col-md-offset-2">
				<hr/>
			</div>

			<h3 style="clear:both">Hieronder kunt u uw profiel aanpassen</h3>

			<div class="col-md-8 col-md-offset-2 form-container">
				{!!	Form::model(Auth::user(), ['route' => ['update', Auth::user()->id], 'method' => 'patch', 'id' => 'update-form'])	!!}
					<div class="form-group">
						{!!	Form::label('name', 'Naam')	!!}
						{!!	Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Vul in voor hoeveel dagen de prijs geldt', 'id' => 'form-name']) !!}
					</div>

					<div class="form-group">
						{!!	Form::submit('Sla veranderingen op', ['class' => 'btn btn-primary form-control form-submit-button']) !!}
					</div>

					<input id="token" type="hidden" value="{{$encrypted_token}}">

				{!!	Form::close() !!}
			</div>
		</div>
@include('layouts/footer')