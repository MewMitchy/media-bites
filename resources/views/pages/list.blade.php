@include('layouts/head')
@include('layouts/header')
<div class="container">
			@if(Auth::user())
				<a class="btn btn-default btn-sm" href="auth/facebook/logout" style="position: absolute;right: 2em;top: 1em;"><i class="glyphicon glyphicon-log-out" style="padding-right: 1em;"></i>Logout</a>
			@endif
			<div class="content container" style="width:100%">
				<div class="title">Media Bites</div>
			</div>

			<ul style="list-style:none;">
				@foreach($users as $user)
					<a href="/user/{{$user->slug}}">
						<div class="profile-list">
							<img src="{{$user->avatar}}" width="50" height="50" class="profile-picture" style="display:inline-block;">
							<li style="display:inline-block;padding-left:0.875em;">{{$user->name}}</li>
						</div>
					</a>
				@endforeach
			</ul>
		</div>

@include('layouts/footer')