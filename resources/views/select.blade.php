@include('layouts/head')
@include('layouts/header')
<div class="container">
			@if(Auth::user())
				<a class="btn btn-default btn-sm" href="auth/facebook/logout" style="position: absolute;right: 2em;top: 1em;"><i class="glyphicon glyphicon-log-out" style="padding-right: 1em;"></i>Logout</a>
			@endif
			<div class="content container" style="width:100%">
				<div class="title">Media Bites</div>
			</div>

			<img src="{{Auth::user()->avatar}}" width="150" height="150" class="profile-picture">
			<h1>Welkom, {{Auth::user()->name}}</h1>
			<div class="col-md-8 col-md-offset-2">
				<hr/>
			</div>

			<h3 style="clear:both">Bent u een student of een professional?</h3>

			<div class="col-md-8 col-md-offset-2 form-container">
				{!!	Form::model(Auth::user(), ['route' => ['update', Auth::user()->id], 'method' => 'patch'])	!!}
					<div class="form-group">
						{!! Form::radio('role', 1, null, ['class' => 'field']) !!} <span style="margin-right:2em;padding-left: 0.4em;color: black;font-size: 1.2em;">Student</span>
						{!! Form::radio('role', 2, null, ['class' => 'field']) !!} <span style="padding-left: 0.4em;color: black;font-size: 1.2em;">Professional</span>
					</div>

					<div class="form-group">
						{!!	Form::submit('Sla op', ['class' => 'btn btn-primary form-control']) !!}
					</div>

				{!!	Form::close() !!}
			</div>
		</div>

@include('layouts/footer')